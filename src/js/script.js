document.addEventListener("DOMContentLoaded", function(event) {

    //Un commentaire
    console.log("Ça fonctionne");


    document.getElementById("connexion-lien").addEventListener('click', function (event) {

        event.preventDefault();

        document.querySelector("#connexion-modale").classList.add("open");

    })

    document.getElementById("fermer-connexion-modale").addEventListener('click', function (event) {


        document.querySelector("#connexion-modale").classList.remove("open");

    })

    var intervalId = 0; // Needed to cancel the scrolling when we're at the top of the page
    var $scrollButton = document.querySelector('.scroll'); // Reference to our scroll button

    function scrollStep() {
        // Check if we're at the top already. If so, stop scrolling by clearing the interval
        if (window.pageYOffset === 0) {
            clearInterval(intervalId);
        }
        window.scroll(0, window.pageYOffset - 50);
    }

    function scrollToTop() {
        // Call the function scrollStep() every 16.66 millisecons
        intervalId = setInterval(scrollStep, 16.66);
    }

// When the DOM is loaded, this click handler is added to our scroll button
    $scrollButton.addEventListener('click', scrollToTop);


});


